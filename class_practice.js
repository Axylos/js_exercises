function Cat(name, owner) { 
  this.owner = owner,
  this.name = name
}

Cat.prototype.cuteStatement = function() {
  return this.owner + " loves " + this.name;
}

var c = new Cat("sennecy", "drake");

var d = new Cat("buttholes", "daniel");

console.log(d.cuteStatement())

Cat.prototype.meow = function() {
  return "puurrrr";
}

console.log(d.meow());

c.meow = function() {
  return "meow, bitches";
}

console.log(c.meow());

console.log(d.meow());