"use strict";

var myUniq = function(arr) {
  var newArray = [];
  for(var i = 0; i < arr.length; i++) {
    var dup = false;
    for(var j = 0; j < newArray.length; j++) {
      if(arr[i] === newArray[j]) {
        dup = true;
      }
    }
    if(dup === false) {
      newArray.push(arr[i]);
    }
  }
  return newArray;
};

//console.log(myUniq([1,2, 2, 3, 3, 4]));

var twoSum = function(arr) {
  var result = [];
  
  for(var i = 0; i < arr.length; i++) {
    for(j = i + 1; j < arr.length; j++) {
      if(arr[i] + arr[j] === 0) {
        result.push([i, j]);
      }
    }
  }
  return result;
}

//console.log(twoSum([-1, 0, 2, -2, 1]));

var myTranspose = function(matrix){
  var newMatrix = [];
  for(var i =0; i < matrix.length; i++){
    newMatrix.push([]);
  }
  
  for(var i =0; i< matrix.length; i++){
    for(var j =0;j<matrix[0].length; j++){
      newMatrix[i][j] = matrix[j][i];
    }
  }
  return newMatrix;
}

//console.log(myTranspose([[1,2,3],[4,5,6],[7,8,9]]));

var multiples = function(arr){
  var newArray= arr.map( function(x) {
    return x * 2;
  });
  return newArray;
}

//console.log(multiples([1,2,3]));

var myEach = function(arr, func) {
  for(var i = 0; i < arr.length; i++) {
    func(arr[i]);
  }
  return arr;
}

//console.log(myEach(([1, 2, 3]), function(x){ console.log(x * 2);}));

var myMap = function(arr, mapFunc) {
  var result = [];
  
  var pushFunc = function(x){
    
    var newX = mapFunc(x);
    console.log(x);
    result.push(newX);
  }
  myEach(arr,pushFunc);
  return result;
}

//console.log(myMap(([1, 2, 3]), function(x){ return x * 2;}));

var myInject = function(arr, injectFunc){
  var accum = arr.shift();
  
  var accuFunc = function(x){
    accum = injectFunc(accum, x);
  }
  
  myEach(arr, accuFunc);
  return accum;
}

//console.log(myInject([1,2,3], function(x,y) {return x+y;}))

var bubblesort = function(arr) {
  var changed = true;
  
  while (changed === true) {
    changed = false;
    
    for(var i = 0; i < arr.length - 1; i++) {
      if(arr[i] > arr[i + 1]) {
        var tmp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = tmp;
        changed = true;
      }
    }
  }
  return arr;
};

//console.log(bubblesort([3, 1, 2, 5, 4]));

var subStrings = function(str) {
  var result = [];
  
  for(var i = 0; i < str.length; i++) {
    for(var j = 0; j < str.length - i; j++) {
      
      result.push(str.slice(i, (i + j + 1)));
    }
  }
  return result;
}

//console.log(subStrings("cats"));

var range = function(lower, higher) {
  
  if (higher < lower){
    return [];
  }else{
    return range(lower, higher-1).concat(higher); 
  }
}

//console.log(range(1,10));

var arraySum = function(arr){
  if (arr.length === 1){
    return arr[0];
  }else{
    return arraySum(arr.slice(0, arr.length - 1)) + arr[arr.length - 1];
  }
}

//console.log(arraySum([1,2,3]));


var exp = function(b, n) { 
  if(n === 0) {
    return 1;
  } else {
    return b * exp(b, n - 1);
  }
};

//console.log(exp(2, 3));

var exp2 = function(b, n) {
  if(n === 0 ) {
    return 1;
  } else if(n === 1) {
      return b;
  } else if(n % 2 === 0) {
    return pow(exp(b, (n / 2)), 2);
  } else {
    return b * ( Math.pow( exp(b, ( (n - 1) / 2) ), 2 ) ) ;
  }
  
}

//console.log(exp2(2, 3));


var fib = function(n) {
  if(n === 0) {
    return 0;
  } else if(n === 1) {
    return 1;
  } else {
    return fib(n - 1) + fib(n - 2);
  }
};

//console.log(fib(4));

var binary = function(arr, target) {
  var mid = Math.floor(arr.length / 2);

  if(arr.length === 1 && target !== arr[0]) {
      return null;
  } else if(arr[mid] === target) {
      return mid;
  } else if(arr[mid] > target) {
      return binary(arr.slice(0, mid), target)
  } else {
      var earlier_result = binary(arr.slice(mid), target)
      if(earlier_result === null) {
        return null;
      } else {
        return  earlier_result + mid;
      }
  } 
} 
 
//console.log(binary([1, 2, 3, 4], 1));
var change = function(amount, coinList){
  var bestCoinList = [];
 
  if (coinList.length == 0 || amount <= 0){
    return [];
  }else{
    
    for (var i = 0; i < coinList.length; i++){
      if (coinList[i] > amount) {
      } else {
        
        var coin = coinList[i];
        var remainder = amount - coin;
        
        var currentCoinList = coinList.slice(i);
        var solution = change(remainder, currentCoinList);
        
        solution.unshift(coin);
        
        if(bestCoinList.length === 0 || bestCoinList.length > solution.length) {
          bestCoinList = solution;
        }
      }
    }
    
  }
  return bestCoinList;
}
  
//console.log(change(14, [10, 7, 1]))

var mergeSort = function(arr){
  if(arr.length === 1 || arr.length === 0){
    return arr;
  }
  else{
    var mid = Math.floor(arr.length /2);
    var left = arr.slice(0, mid);
    var right = arr.slice(mid);
    return merge(mergeSort(left), mergeSort(right));
  }
}

var merge = function(l, r){

  var result = [];

  while (l.length > 0 && r.length > 0){
    if (l[0] > r[0]){
      result.push(r.shift());
    }else {
      result.push(l.shift());
    }
    
  }
  result = result.concat(l).concat(r);
  return result;
}

//console.log(mergeSort([4, 2, 7, 3, 5, 1, 6]));

var subsets = function(arr){
  if (arr.length === 0){
    return [[]];
  }else{
    var without = subsets(arr.slice(1));
    var withFirst = without.map(function(x) {
      console.log(x);
      //return x.push(arr[0]);
    })
    return without.concat(withFirst);
  }   
}

console.log(subsets([1,2,3]));