"use strict";

function Student(firstName, lastName) {
  this.firstName = firstName,
  this.lastName = lastName
  this.enrolledCourses = []
}

Student.prototype.name = function() {
  return this.firstName + " " + this.lastName;
}

Student.prototype.courses = function() {
  if(this.enrolledCourses.length > 0) { 
    var courseNames = this.enrolledCourses.map(function(course) {
      return course.name;
    })
    return courseNames.join(", ");
  } else {
    return "Not enrolled in any courses!";
  }
}

function Course(name, department, credits) {
  this.name = name,
  this.department = department,
  this.credits = credits
  this.enrolledStudents = []
}

Course.prototype.students = function() {
  var studentNames = this.enrolledStudents.map(function(student) {
    return student.firstName + " " + student.lastName;
  })
  return studentNames.join(", ");
}

Student.prototype.enroll = function(course) {
  var currentStudent = this
  var alreadyEnrolled = course.enrolledStudents.some(function(student) {
    return student == currentStudent;
  })
  
  if(!alreadyEnrolled) {
    this.enrolledCourses.push(course);
    course.enrolledStudents.push(this);
  } else {
    
    console.log("fuck off");
  }
}

Student.prototype.courseLoad = function() {
  var load = {};
  this.enrolledCourses.forEach(function(course) {
    if(load[course.department]) {
      load[course.department] += course.credits;
    } else {
      load[course.department] = course.credits;
    }
  });
  
  return load;
}

var me = new Student("bobby", "talley");
var bobby = new Student("drake", "robert");

var bio = new Course("biology", "science", 4);
var physics = new Course("biology", "science", 2);

var philosophy = new Course("philosophy", "humanities", 6);
var literature = new Course("literature", "humanities", 2);

me.enroll(philosophy);
me.enroll(literature);

bobby.enroll(philosophy);

//console.log(philosophy.students());
//console.log(me.courses());

me.enroll(philosophy);

console.log(me.courseLoad());

me.enroll(bio);

console.log(me.courseLoad());
